import { Injectable } from '@angular/core';

@Injectable()
export class ClienteService {

  public clientes = [
    {
      id:1,
      nombre:"Felipe",
      apellido1:"Martínez",
      apellido2:"Barahona",
      descripcion:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ",
      foto:"https://gitlab.com/uploads/-/system/user/avatar/1230193/avatar.png"
    },
    {
      id:2,
      nombre:"Brayan",
      apellido1:"Arrieta",
      apellido2:"Alfaro",
      descripcion:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ",
      foto:"https://gitlab.com/uploads/-/system/user/avatar/1348876/avatar.png"
    },
    {
      id:3,
      nombre:"Miguel",
      apellido1:"Méndez",
      apellido2:"Rojas",
      descripcion:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ",
      foto:"https://gitlab.com/uploads/-/system/user/avatar/830273/avatar.png"
    },
    {
      id:4,
      nombre:"Andrey",
      apellido1:"Murillo",
      apellido2:"Castro",
      descripcion:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ",
      foto:"https://gitlab.com/uploads/-/system/user/avatar/754347/avatar.png"
    },  
  ];

  private selectedId:number = null;

  constructor() { }

  getCliente(id:number){
    return this.clientes.filter(cliente=>cliente.id==id)[0];
  }

  getActual(){
    return this.selectedId;
  }

  setActual(id:number){
    this.selectedId = id;
  }

}
