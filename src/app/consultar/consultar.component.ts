import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../cliente.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-consultar',
  templateUrl: './consultar.component.html',
  styleUrls: ['./consultar.component.css']
})
export class ConsultarComponent implements OnInit {

  private cliente:any;

  constructor(private service:ClienteService, private _route:ActivatedRoute) { }

  ngOnInit() {
    const id = +this._route.snapshot.paramMap.get('id');
    this.service.setActual(id);
    this.cliente = this.service.getCliente(id);
    console.log(this.cliente);
    
  }

  getNombreCompleto(){
    return `${this.cliente.nombre} ${this.cliente.apellido1} ${this.cliente.apellido2}`;
  }

}
