import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../cliente.service';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})


export class ListarComponent implements OnInit {

  private clientes;

  constructor(private service: ClienteService) { }

  ngOnInit() {
    this.clientes = this.service.clientes;
  }

  getActual(){
    return this.service.getActual();
  }
  restart(){
    this.service.setActual(null);
  }

}
